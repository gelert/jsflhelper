Useful methods for haxe jsfl.

**relativeToAbsoluteURI** - converts your relative URI to absolute URI.
It's useful because jsfl hasn't recognized relative URI's since a long time ago (it's bug);