package com.gelert.jsflhelper;

import jsfl.Flash;

using StringTools;
using haxe.io.Path;

/**
 * ...
 * @author gelert
 */
class JSFLHelper {
	
	static inline var _PREFIX = 'file:///';
	
	public static function relativeToAbsoluteURI(relative:String):Null<String> {
		if(!relative.startsWith(_PREFIX)) return null;
		var path = Flash.scriptURI;
		var index = path.lastIndexOf('/');
		path = path.substring(_PREFIX.length, index + 1);
		relative = relative.replace(_PREFIX, '');
		path += relative;
		path = path.normalize();
		return _PREFIX + path;
	}
}